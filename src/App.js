import logo from './logo.svg';
import './App.css';
import Box from './Box';
import { useState } from 'react';
import { HTML5Backend } from 'react-dnd-html5-backend'
import { DndProvider } from 'react-dnd'

function App() {


  const [cards, setCards] = useState([
    {
      id: 1,
      box: 1
    },
    {
      id: 2,
      box: 2
    }
  ]);

  const moveIt = (id, box) => {
    const cardIndex = cards.findIndex(c => c.id == id);
    const newCards = [...cards];
    newCards[cardIndex].box = box;
    console.log(newCards);
    setCards(newCards);
  }

  return (
    <div className="App">
      <DndProvider backend={HTML5Backend}>
        <div className="grid">
          {
              [0,1,2,3].map((i)=> {
                return (<Box cards={cards.filter(c => c.box === i)} box={i} moveIt={moveIt}>
                </Box>)
              })
          }
          
        </div>
      </DndProvider>
    </div>
  );
}

export default App;
