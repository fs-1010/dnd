import './App.css';
import Card from './Card';
import { useState } from 'react';
import { HTML5Backend } from 'react-dnd-html5-backend'
import { DndProvider } from 'react-dnd'
import { useDrop } from 'react-dnd'

function Box({cards, box, moveIt}) {

  const [collectedProps, drop] = useDrop(() => ({
    accept: 'card',
    drop: (i) => moveIt(i.id, box)
  }))

  return (
    <div className="box" ref={drop}>
        {cards.map((c) => <Card box={c.box} id={c.id}></Card>)}
    </div>
  );
}

export default Box;
