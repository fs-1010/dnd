import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import { useDrag } from 'react-dnd'

function Card({box, id}) {

    console.log(box);
    const [{isDragging}, drag] = useDrag(() => ({
        type: 'card',
        item: {id},
        collect: monitor => ({
            isDragging: !!monitor.isDragging(),
          }),
      }))

      console.log(isDragging)
  return (
    <div ref={drag}>
        {box}
    </div>
  );
}

export default Card;
